#!/bin/bash

echo "Killing and removing dockers"
docker-compose -f docker-compose-nfr.yml kill && docker-compose -f docker-compose-nfr.yml down -v --remove-orphans
docker-compose -f docker-compose-initializer.yml down -v
rm -rf /tmp/dataClay-edge/storage/*
mkdir -p /tmp/dataClay-edge/storage

echo "Launching dockers"
docker-compose -f docker-compose-nfr.yml up -d

# check if logicmodule and backend are ready for initializer
#res=$(docker-compose logs 2>&1 | grep " ===> The ContractID for the registered classes is:")
res=$(docker-compose -f docker-compose-nfr.yml logs 2>&1 | grep "Registered StorageLocation named")
while [ -z "$res" ];
do
        echo "Dataclay logicmodule not ready yet. Waiting for it to finish..."
        sleep 3
        res=$(docker-compose -f docker-compose-nfr.yml logs 2>&1 | grep "Registered StorageLocation named")
done
echo "Dataclay logicmodule ready, initializer can start."


docker-compose -f docker-compose-initializer.yml up -d
# check if dockers already up and registered
#res=$(docker-compose logs 2>&1 | grep " ===> The ContractID for the registered classes is:")
res=$(docker-compose -f docker-compose-initializer.yml logs 2>&1 | grep "+ echo READY")
first_iter=true
# first_iter=false
while [ -z "$res" ];
do
        echo "Dataclay initializer not ready yet. Waiting for it to finish..."
	if [ "$first_iter" = true ];
	then
		first_iter=false
		echo "Launching backends for 192.168.121.208 and 192.168.121.209"
		ssh flo01@192.168.121.208 "cd deploy-bsc/ && bash launch_dataclay.sh"
		ssh flo01@192.168.121.209 "cd deploy-bsc/ && bash launch_dataclay.sh"
	fi
        sleep 3
        res=$(docker-compose -f docker-compose-initializer.yml logs 2>&1 | grep "+ echo READY")
done
echo "Dataclay initializer registered model, stubs can be retrieved."

echo "Deploying GRM and NFR Tools in Master 192.168.121.210"
cd ../
./launch_nfrs.sh

echo "Deploying NFR Tools in 192.168.121.208"
ssh flo01@192.168.121.208 "cd deploy-bsc/ && bash launch_nfrs.sh"
echo "Deploying NFR Tools in 192.168.121.209"
ssh flo01@192.168.121.209 "cd deploy-bsc/ && bash launch_nfrs.sh"
