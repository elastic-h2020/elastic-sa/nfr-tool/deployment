#!/bin/bash

# Deploy the GRM without dataClay (already deployed with the smart city COMPSs dockers)
echo "Deploying the GRM...."
cd ../grm-global_resource_manager/
docker-compose down -v --remove-orphans
docker-compose up --build -d

# Deploy the NFR COMMS without dataClay
echo "Deploying the NFR Comms...."
cd ../nfrtool-comms/
./launch_dockers.sh

# Deploy NFR time and energy without dataClay
echo "Deploying the NFR Time and Energy"
# cd ../nfrtool-time-and-energy
cd ../nfrtool-te-fake2/
./launch_dockers.sh
cd -
